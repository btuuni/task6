package com.university.task6

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        lateinit var dbHandler: DBHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHandler = DBHandler(this, null, null, 1)

        viewItems()

        btnAddItem.setOnClickListener(){
            val intent = Intent(this, AddItemActivity::class.java)
            startActivity(intent)
        }
    }


    @SuppressLint("WrongConstant")
    private fun viewItems(){
        val itemsList = dbHandler.getItems(this)
        val adapter = TaskAdapter(this, itemsList)
        val recycler_view : RecyclerView = findViewById(R.id.recycler_view)
        recycler_view.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager
        recycler_view.adapter = adapter
    }

    override fun onResume() {
        viewItems()
        super.onResume()
    }
}
