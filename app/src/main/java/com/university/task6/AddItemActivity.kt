package com.university.task6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_item.*

class AddItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)

        btnSave.setOnClickListener(){
            if (editTitle.text.isEmpty()){
                Toast.makeText(this, "Enter item title", Toast.LENGTH_SHORT).show()
            } else{
                val item = Item()
                item.itemTitle = editTitle.text.toString()
                if (editDesc.text.isEmpty())
                    item.itemDesc = "" else
                    item.itemDesc = editDesc.text.toString()
                MainActivity.dbHandler.addItem(this, item)
            }

        }


    }
}
