package com.university.task6

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item.view.*


class TaskAdapter(ctx : Context, val items : ArrayList<Item>) : RecyclerView.Adapter<TaskAdapter.ViewHolder>() {


    val ctx = ctx

    class  ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val itemTitle = itemView.itemTitle
        val itemDescription = itemView.itemDesc
        val btnUpdate = itemView.btnUpdate
        val btnDelete = itemView.btnDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskAdapter.ViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false )
        return ViewHolder(a)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: TaskAdapter.ViewHolder, position: Int) {
        val item : Item = items[position]
        holder.itemTitle.text = item.itemTitle
        holder.itemDescription.text = item.itemDesc

        holder.btnDelete.setOnClickListener(){
            val itemTitle = item.itemTitle
            if (MainActivity.dbHandler.deleteItem(item.itemID)){
                items.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeRemoved(position, items.size)
            }
        }
//        holder.btnUpdate.setOnClickListener(){
//            val itemTitle = item.itemTitle
//            if (MainActivity.dbHandler.updateItem(item.itemID)) {
//                items.
//            }
//
//
//        }

    }
}